package cartes.main;

import cartes.Paquet;
import cartes.Paquet32;
import cartes.carte.Carte32;
import cartes.carte.ICarte;

import java.util.Scanner;

public class PlusOuMoins<T extends ICarte> {

    private static final int SCORE_INITIAL = 10;
    private static final int GAIN = 1;
    private static final int PERTE = 2;

    private final Paquet<T> paquet;
    private int score = SCORE_INITIAL;

    public PlusOuMoins(Paquet<T> p) {
        this.paquet = p;
    }

    public void jouer() {
        System.out.println("Bienvenue au jeu du plus ou moins, jouer ? o/n");

        Scanner sc = new Scanner(System.in);
        String rep = sc.next();

        boolean fini = (rep.compareTo("n") == 0);
        boolean coupGagnant = true;

        int indice = 0;

        ICarte carteCourante = paquet.get(indice);
        carteCourante.tourner();

        while (!fini) {
            System.out.println(paquet);
            rep = lireComparaison(sc);

            ICarte prochaineCarte = paquet.get(indice+1);
            prochaineCarte.tourner();

            switch (rep) {
                case "q":
                    fini = true;
                    System.out.println("Au revoir");
                    break;
                case "+":
                    coupGagnant = prochaineCarte.estSuperieureOuEgale(carteCourante);
                    break;
                case "-":
                    coupGagnant = carteCourante.estSuperieureOuEgale(prochaineCarte);
                    break;
                default:
                    System.out.println("Les choix possibles sont +/-/q");
                    coupGagnant = false;
            }

            if (!fini) {
                if (coupGagnant) {
                    System.out.println("coup gagnant");
                    gain();
                } else {
                    System.out.println("coup perdant");
                    perte();
                }

                carteCourante.tourner();
                indice++;

                fini = (score == 0) || (indice == paquet.size() - 1);

                if (!fini) {
                    carteCourante = paquet.get(indice);
                } else {
                    System.out.println(paquet);
                }
            }

            System.out.println("score = " + score);
        }
    }

    private String lireComparaison(Scanner sc) {
        System.out.println("la prochaine carte est-elle supérieure + ou inférieure - ? q pour quitter");
        return sc.next();
    }

    public void gain() {
        score += GAIN;
    }

    public void perte() {
        score -= PERTE;
    }

    public static void main(String[] args) {
//        Paquet52 paquet52 = Paquet52.getInstance();
//        PlusOuMoins<Carte52> plusOuMoins52 = new PlusOuMoins<Carte52>(paquet52);
//        plusOuMoins52.jouer();

        Paquet32 paquet32 = Paquet32.getInstance();
        PlusOuMoins<Carte32> plusOuMoins32 = new PlusOuMoins<Carte32>(paquet32);
        plusOuMoins32.jouer();
    }
}
