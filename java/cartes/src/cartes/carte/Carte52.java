package cartes.carte;

import cartes.carte.enums.Atout;
import cartes.carte.enums.Valeur;

public class Carte52  extends CarteClassique {

    private static final String DOS="[----]";

    public Carte52(Valeur valeur, Atout atout) {
        super(DOS, valeur, atout);
    }
}
