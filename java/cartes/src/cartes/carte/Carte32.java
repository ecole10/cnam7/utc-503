package cartes.carte;

import cartes.carte.enums.Atout;
import cartes.carte.enums.Valeur;

public class Carte32 extends CarteClassique {

    private static final String DOS="[****]";

    public Carte32(Valeur valeur, Atout atout) {
        super(DOS, valeur, atout);

        if (valeur.ordinal() < Valeur.SEPT.ordinal()) {
            System.out.println("Valeur non autorisée");
        }
    }
}
