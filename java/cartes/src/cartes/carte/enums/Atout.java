package cartes.carte.enums;

public enum Atout {
    PIQUE("♠"),
    COEUR("♥"),
    CARREAU("♦"),
    TREFLE("♣");

    private final String visuel;

    Atout(String visuel) {
        this.visuel = visuel;
    }

    @Override
    public String toString() {
        return visuel;
    }
}
