package cartes.carte.enums;

public enum Valeur {
    DEUX("2"),
    TROIS("3"),
    QUATRE("4"),
    CINQ("5"),
    SIX("6"),
    SEPT("7"),
    HUIT("8"),
    NEUF("9"),
    DIX("10"),
    VALET("V"),
    DAME("D"),
    ROI("R"),
    AS("AS");

    private final String label;

    Valeur(String label) {
        this.label = label;
    }

    public String toString() {
        return label;
    }

    public static Valeur get(int i) {
        return Valeur.values()[i];
    }
}
