package cartes.carte;

public interface ICarte {
    boolean estSuperieureOuEgale(ICarte carte);
    void tourner();
}
