package cartes;

import cartes.carte.Carte32;
import cartes.carte.enums.Atout;
import cartes.carte.enums.Valeur;

import java.util.ArrayList;

public class Paquet32 extends Paquet<Carte32> {

    private static Paquet32 instance = null;

    private Paquet32() {
        super();
    }

    public static Paquet32 getInstance() {
        if (null == instance) {
            instance = new Paquet32();
        }

        return instance;
    }

    protected void creerPaquet() {
        super.cartes = new ArrayList<Carte32>(32);
        for (Atout atout : Atout.values()) {
            for (int i = Valeur.SEPT.ordinal(); i <= Valeur.AS.ordinal(); i++) {
                Valeur valeur = Valeur.get(i);
                super.add(new Carte32(valeur, atout));
            }
        }
    }

    @Override
    public String toString() {
        return super.toString(4);
    }
}
