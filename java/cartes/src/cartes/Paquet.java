package cartes;

import java.util.Collections;
import java.util.List;

import cartes.carte.ICarte;

public abstract class Paquet<T extends ICarte> {

    protected List<T> cartes;

    protected abstract void creerPaquet();

    public Paquet() {
        super();
        creerPaquet();
        this.shuffle();
    }

    public int size() {
        return cartes.size();
    }

    public boolean add(T uneCarte) {
        return cartes.add(uneCarte);
    }

    public T get(int i) {
        return cartes.get(i);
    }

    protected String toString(int nbColonnes) {
        StringBuilder rep = new StringBuilder();

        for (int i = 0; i < size(); i++) {
            T elem = cartes.get(i);
            rep.append(elem.toString());

            if (i % nbColonnes == (nbColonnes - 1)) {
                rep.append("\n");
            }
        }

        return rep.toString();
    }

    @Override
    public String toString() {
        return this.toString(5);
    }

    public void shuffle() {
        Collections.shuffle(cartes);
    }
}
