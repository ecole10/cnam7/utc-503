package cartes;

import cartes.carte.Carte52;
import cartes.carte.enums.Atout;
import cartes.carte.enums.Valeur;

import java.util.ArrayList;

public class Paquet52 extends Paquet<Carte52> {

    private static Paquet52 instance = null;

    private Paquet52() {
        super();
    }

    public static Paquet52 getInstance() {
        if (null == instance) {
            instance = new Paquet52();
        }

        return instance;
    }

    protected void creerPaquet() {
        super.cartes= new ArrayList<Carte52>(52);
        for (Atout atout : Atout.values()) {
            for (Valeur valeur : Valeur.values()) {
                super.add(new Carte52(valeur, atout));
            }
        }
    }

    @Override
    public String toString() {
        return super.toString(6);
    }
}
